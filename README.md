# Unity CI

This project contains all the necessary [GitLab](https://gitlab.com) CI yml scripts and bash scripts required to run a CI for [Unity](https://unity.com/) projects.

## Getting Started

### Prerequisites

- [GitLab](https://gitlab.com) account
- [Unity](https://unity.com/) account
- A Unity project hosted on GitLab

### Installation

#### Set CI variables

Go to `Settings > CI/CD > Variables` and set
- `UNITY_USERNAME`: The username (not the email) from your Unity account
- `UNITY_PASSWORD`: The password from your Unity account
- `UNITY_LICENSE_CONTENT`: The content of the license required to activate Unity. If you do not have any license content yet, do not set this variable, you will be told how to do this later.

#### Add Unity CI submodule

Add `unity-ci` repository as one of your project submodule under `ci` folder:

```bash
git submodule add git@gitlab.com:codingones/games/unity-ci.git ci
```

#### Add CI entry point

In `.gitlab-ci.yml`, paste the following content:
```yml
variables:
  CI_PATH: ./ci
  GIT_SUBMODULE_STRATEGY: recursive

include:
  - project: 'codingones/games/unity-ci'
    ref: master
    file: 'unity-ci-template.yml'
```

#### Activate Unity

Without `UNITY_LICENSE_CONTENT` variable, `get-activation-file` job is available. After you run this job, `download` the `job artifacts`: it should contain `unity3d.alf` file.  
Under `Settings > CI/CD > Variables`, add a new variable with `UNITY_LICENSE_CONTENT` as key and the content of `unity3d.alf` file as value.

## References

- [Unity CI Github repositories](https://github.com/Unity-CI): Open source continuous integration for Unity
- [Unity CI Docker repositories](https://hub.docker.com/u/unityci): Docker image repositories for Unity hub and editor
- [Unity GitLab CI Example](https://gitlab.com/gableroux/unity3d-gitlab-ci-example): a repository that contains a full working example of a CI that build, test and deploy a Unity project.
